var saved_script_objects = {
	"foobar_this_is_a_comment": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "foobar this is a comment",
		"object_library": "Chrome HTML",
		"window_name": "Hello world! – TTE Site Title",
		"xpath": "//article[@id='div-comment-5']/div[1]/p",
		"title": "Hello world! – TTE Site Title",
		"url": "http://localhost:8080/?p=1#comment-2",
		"fullpath": {
			"path": [
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 0,
					"text": "foobar this is a comment 1569341268"
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "comment-content"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "article",
					"attributes": [
						{
							"name": "id",
							"value": "div-comment-5"
						},
						{
							"name": "class",
							"value": "comment-body"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "comment-5"
						},
						{
							"name": "class",
							"value": "comment byuser comment-author-wp_admin bypostauthor odd alt thread-odd thread-alt depth-1"
						}
					],
					"indexInParent": 1,
					"level": 3
				},
				{
					"tagName": "ol",
					"attributes": [
						{
							"name": "class",
							"value": "comment-list"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "comments"
						},
						{
							"name": "class",
							"value": "comments-area"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "main",
					"attributes": [
						{
							"name": "id",
							"value": "main"
						},
						{
							"name": "class",
							"value": "site-main"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "primary"
						},
						{
							"name": "class",
							"value": "content-area"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "content"
						},
						{
							"name": "class",
							"value": "site-content"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "site-inner"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "page"
						},
						{
							"name": "class",
							"value": "site"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "single single-post postid-1 single-format-standard logged-in admin-bar  customize-support"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "class",
							"value": "js"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 12
				}
			]
		},
		"screenshot": "Reports\\39f67cde-23cc-9968-0e47-37d726bc07e7.png"
	}
}