var saved_script_objects={
	"Username_or_Email": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Username or Email\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='log' and @id='user_login']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "text"
						},
						{
							"name": "name",
							"value": "log"
						},
						{
							"name": "id",
							"value": "user_login"
						},
						{
							"name": "aria-describedby",
							"value": "login_error"
						},
						{
							"name": "class",
							"value": "input"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "size",
							"value": "20"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "user_login"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\6588030b-e8f6-ba95-f76f-f21d4f94c0d1.png"
	},
	"Password": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Password\n\t\t",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='pwd' and @id='user_pass']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "password"
						},
						{
							"name": "name",
							"value": "pwd"
						},
						{
							"name": "id",
							"value": "user_pass"
						},
						{
							"name": "aria-describedby",
							"value": "login_error"
						},
						{
							"name": "class",
							"value": "input"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "size",
							"value": "20"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "label",
					"attributes": [
						{
							"name": "for",
							"value": "user_pass"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\8ecccb47-75b4-36dd-51e4-59daf22bb527.png"
	},
	"wp_submit": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Button",
		"object_name": "wp-submit",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//input[@name='wp-submit' and @id='wp-submit']",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "submit"
						},
						{
							"name": "name",
							"value": "wp-submit"
						},
						{
							"name": "id",
							"value": "wp-submit"
						},
						{
							"name": "class",
							"value": "button button-primary button-large"
						},
						{
							"name": "value",
							"value": "Log In"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "submit"
						}
					],
					"indexInParent": 3,
					"level": 1
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "loginform"
						},
						{
							"name": "id",
							"value": "loginform"
						},
						{
							"name": "action",
							"value": "http://localhost:8080/wp-login.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "_lpchecked",
							"value": "1"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 5
				}
			]
		},
		"screenshot": "Recording\\a0783fc0-4028-137a-c391-d47c87e58127.png"
	},
	"Plugins_1": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Cell",
		"object_name": "Plugins 1",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='menu-plugins']/a/div[3]",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/",
		"fullpath": {
			"path": [
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wp-menu-name"
						}
					],
					"indexInParent": 2,
					"level": 0
				},
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "plugins.php"
						},
						{
							"name": "class",
							"value": "wp-has-submenu wp-not-current-submenu menu-top menu-icon-plugins"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						}
					],
					"indexInParent": 0,
					"level": 1,
					"text": "\n\nPlugins \n1"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "class",
							"value": "wp-has-submenu wp-not-current-submenu menu-top menu-icon-plugins"
						},
						{
							"name": "id",
							"value": "menu-plugins"
						}
					],
					"indexInParent": 8,
					"level": 2
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenu"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenuwrap"
						}
					],
					"indexInParent": 1,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenumain"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Main menu"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Reports\\9b88caa7-bad6-084e-7c00-75a2bbe2df3e.png"
	},
	"Installed_Plugins": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Installed Plugins",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='menu-plugins']/ul/li[2]/a",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "plugins.php"
						},
						{
							"name": "class",
							"value": "wp-first-item"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Installed Plugins"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "class",
							"value": "wp-first-item"
						}
					],
					"indexInParent": 1,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "class",
							"value": "wp-submenu wp-submenu-wrap"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "class",
							"value": "wp-has-submenu wp-not-current-submenu menu-top menu-icon-plugins opensub"
						},
						{
							"name": "id",
							"value": "menu-plugins"
						}
					],
					"indexInParent": 8,
					"level": 3
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenu"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenuwrap"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenumain"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Main menu"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Recording\\85035bae-53d5-f375-2c53-55eb82bc5d2e.png"
	},
	"Hello_Dolly": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "Hello Dolly",
		"object_library": "Chrome HTML",
		"window_name": "Plugins ‹ TTE Site Title — WordPress",
		"xpath": "//tr[3]/td[1]/strong",
		"title": "Plugins ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/plugins.php",
		"fullpath": {
			"path": [
				{
					"tagName": "strong",
					"attributes": [],
					"indexInParent": 0,
					"level": 0,
					"text": "Hello Dolly"
				},
				{
					"tagName": "td",
					"attributes": [
						{
							"name": "class",
							"value": "plugin-title column-primary"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "tr",
					"attributes": [
						{
							"name": "class",
							"value": "inactive"
						},
						{
							"name": "data-slug",
							"value": "hello-dolly"
						},
						{
							"name": "data-plugin",
							"value": "hello.php"
						}
					],
					"indexInParent": 2,
					"level": 2
				},
				{
					"tagName": "tbody",
					"attributes": [
						{
							"name": "id",
							"value": "the-list"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "table",
					"attributes": [
						{
							"name": "class",
							"value": "wp-list-table widefat plugins"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "bulk-action-form"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 12
				}
			]
		},
		"screenshot": "Reports\\6e8f9616-e40e-72dc-0e1f-901f6c4799e9.png"
	},
	"Activate": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Activate",
		"object_library": "Chrome HTML",
		"window_name": "Plugins ‹ TTE Site Title — WordPress",
		"xpath": "//tr[3]/td[1]/div/span[1]/a",
		"title": "Plugins ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/plugins.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "plugins.php?action=activate&plugin=hello.php&plugin_status=all&paged=1&s&_wpnonce=d6199670f1"
						},
						{
							"name": "class",
							"value": "edit"
						},
						{
							"name": "aria-label",
							"value": "Activate Hello Dolly"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Activate"
				},
				{
					"tagName": "span",
					"attributes": [
						{
							"name": "class",
							"value": "activate"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "row-actions visible"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "td",
					"attributes": [
						{
							"name": "class",
							"value": "plugin-title column-primary"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "tr",
					"attributes": [
						{
							"name": "class",
							"value": "inactive"
						},
						{
							"name": "data-slug",
							"value": "hello-dolly"
						},
						{
							"name": "data-plugin",
							"value": "hello.php"
						}
					],
					"indexInParent": 2,
					"level": 4
				},
				{
					"tagName": "tbody",
					"attributes": [
						{
							"name": "id",
							"value": "the-list"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "table",
					"attributes": [
						{
							"name": "class",
							"value": "wp-list-table widefat plugins"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "bulk-action-form"
						}
					],
					"indexInParent": 1,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 11
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 13
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 14
				}
			]
		},
		"screenshot": "Recording\\9e794c18-1982-f236-8633-8a7f04170e69.png"
	},
	"activated": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "activated",
		"object_library": "Chrome HTML",
		"window_name": "Plugins ‹ TTE Site Title — WordPress",
		"xpath": "//div[@id='message']/p/strong",
		"title": "Plugins ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/plugins.php?plugin_status=all&paged=1&s",
		"fullpath": {
			"path": [
				{
					"tagName": "strong",
					"attributes": [],
					"indexInParent": 0,
					"level": 0,
					"text": "activated"
				},
				{
					"tagName": "p",
					"attributes": [],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "message"
						},
						{
							"name": "class",
							"value": "updated notice is-dismissible"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 9
				}
			]
		},
		"screenshot": "Reports\\dec60eb6-aa3c-f7ee-04f1-6a1878145cd3.png"
	},
	"Dashboard": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Dashboard",
		"object_library": "Chrome HTML",
		"window_name": "Plugins ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='menu-dashboard']/a",
		"title": "Plugins ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/plugins.php?plugin_status=all&paged=1&s",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "index.php"
						},
						{
							"name": "class",
							"value": "wp-first-item wp-has-submenu wp-not-current-submenu menu-top menu-top-first menu-icon-dashboard menu-top-last"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "\n\nDashboard"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "class",
							"value": "wp-first-item wp-has-submenu wp-not-current-submenu menu-top menu-top-first menu-icon-dashboard menu-top-last opensub"
						},
						{
							"name": "id",
							"value": "menu-dashboard"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenuwrap"
						}
					],
					"indexInParent": 1,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "adminmenumain"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Main menu"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 7
				}
			]
		},
		"screenshot": "Recording\\a604d05e-1c6f-c49c-79f8-8e6470c40269.png"
	},
	"You_re_still_glowin___you_re_sti": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "You’re still glowin’, you’re still crowin’",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//p[@id='dolly']",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/index.php",
		"fullpath": {
			"path": [
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "id",
							"value": "dolly"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "You’re still glowin’, you’re still crowin’"
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				}
			]
		},
		"screenshot": "Recording\\45f15ac0-5e0f-fccc-f733-4b5d9ffaec7e.png"
	},
	"Howdy__wp_admin": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "Chrome Legacy Window",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Howdy, wp_admin",
		"object_library": "Chrome HTML",
		"window_name": "Dashboard ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-my-account']/a",
		"title": "Dashboard ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/index.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "aria-haspopup",
							"value": "true"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-admin/profile.php"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Howdy, wp_admin"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-my-account"
						},
						{
							"name": "class",
							"value": "menupop with-avatar hover"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-top-secondary"
						},
						{
							"name": "class",
							"value": "ab-top-secondary ab-top-menu"
						}
					],
					"indexInParent": 1,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 8
				}
			]
		},
		"screenshot": "Recording\\8b50a3b9-26de-9013-b8a1-74d496efff64.png"
	},
	"Log_Out": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "Log Out",
		"object_library": "Chrome HTML",
		"window_name": "Profile ‹ TTE Site Title — WordPress",
		"xpath": "//li[@id='wp-admin-bar-logout']/a",
		"title": "Profile ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/profile.php",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "class",
							"value": "ab-item"
						},
						{
							"name": "href",
							"value": "http://localhost:8080/wp-login.php?action=logout&_wpnonce=114fd42c06"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "Log Out"
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-logout"
						}
					],
					"indexInParent": 2,
					"level": 1
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-user-actions"
						},
						{
							"name": "class",
							"value": "ab-submenu"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "ab-sub-wrapper"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "li",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-my-account"
						},
						{
							"name": "class",
							"value": "menupop with-avatar hover"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "ul",
					"attributes": [
						{
							"name": "id",
							"value": "wp-admin-bar-top-secondary"
						},
						{
							"name": "class",
							"value": "ab-top-secondary ab-top-menu"
						}
					],
					"indexInParent": 1,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "quicklinks"
						},
						{
							"name": "id",
							"value": "wp-toolbar"
						},
						{
							"name": "role",
							"value": "navigation"
						},
						{
							"name": "aria-label",
							"value": "Toolbar"
						},
						{
							"name": "tabindex",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpadminbar"
						},
						{
							"name": "class",
							"value": ""
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 10
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 11
				}
			]
		},
		"screenshot": "Recording\\57e957e3-19c5-e732-7753-20f0830a63ce.png"
	},
	"You_are_now_logged_out_": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "\tYou are now logged out.\n",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Generic",
		"object_name": "You are now logged out.",
		"object_library": "Chrome HTML",
		"window_name": "TTE Site Title ‹ Log In",
		"xpath": "//div[@id='login']/p[1]",
		"title": "TTE Site Title ‹ Log In",
		"url": "http://localhost:8080/wp-login.php?loggedout=true",
		"fullpath": {
			"path": [
				{
					"tagName": "p",
					"attributes": [
						{
							"name": "class",
							"value": "message"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "login"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "login login-action-login wp-core-ui  locale-en-us"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 3
				}
			]
		},
		"screenshot": "Recording\\1a3713f4-585c-f2e6-ec4c-3c1183ab1dd6.png"
	}
};