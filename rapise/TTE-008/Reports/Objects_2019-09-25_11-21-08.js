var saved_script_objects = {
	"Enter_title_here": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "param:object_class",
		"object_text": "Add New Post ‹ TTE Site Title — WordPress - Google Chrome",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_WidgetWin_1",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Text",
		"object_name": "Enter title here",
		"object_library": "Chrome HTML",
		"window_name": "Add New Post ‹ TTE Site Title — WordPress",
		"xpath": "//input[@name='post_title' and @id='title']",
		"title": "Add New Post ‹ TTE Site Title — WordPress",
		"url": "http://localhost:8080/wp-admin/post-new.php",
		"fullpath": {
			"path": [
				{
					"tagName": "input",
					"attributes": [
						{
							"name": "type",
							"value": "text"
						},
						{
							"name": "name",
							"value": "post_title"
						},
						{
							"name": "size",
							"value": "30"
						},
						{
							"name": "value",
							"value": ""
						},
						{
							"name": "id",
							"value": "title"
						},
						{
							"name": "spellcheck",
							"value": "true"
						},
						{
							"name": "autocomplete",
							"value": "off"
						}
					],
					"indexInParent": 0,
					"level": 0
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "titlewrap"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "titlediv"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body-content"
						},
						{
							"name": "style",
							"value": "position: relative;"
						}
					],
					"indexInParent": 0,
					"level": 3
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "post-body"
						},
						{
							"name": "class",
							"value": "metabox-holder columns-2"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "poststuff"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "form",
					"attributes": [
						{
							"name": "name",
							"value": "post"
						},
						{
							"name": "action",
							"value": "post.php"
						},
						{
							"name": "method",
							"value": "post"
						},
						{
							"name": "id",
							"value": "post"
						},
						{
							"name": "autocomplete",
							"value": "off"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "wrap"
						}
					],
					"indexInParent": 3,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody-content"
						},
						{
							"name": "aria-label",
							"value": "Main content"
						},
						{
							"name": "tabindex",
							"value": "0"
						},
						{
							"name": "style",
							"value": "overflow: hidden;"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpbody"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 1,
					"level": 9
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpcontent"
						}
					],
					"indexInParent": 1,
					"level": 10
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "wpwrap"
						}
					],
					"indexInParent": 0,
					"level": 11
				},
				{
					"tagName": "body",
					"attributes": [],
					"indexInParent": 0,
					"level": 12
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "xmlns",
							"value": "http://www.w3.org/1999/xhtml"
						},
						{
							"name": "class",
							"value": "wp-toolbar"
						},
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 13
				}
			]
		},
		"screenshot": "Reports\\fe55b039-148d-3ab3-4bdb-812a2d51ec4b.png"
	}
}