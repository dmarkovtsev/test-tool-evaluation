var saved_script_objects = {
	"First_Blog_Post": {
		"locations": [
			{
				"locator_name": "HTML",
				"location": {
					"xpath": "param:xpath",
					"url": "param:url",
					"title": "param:title"
				}
			},
			{
				"locator_name": "HTML FPP",
				"location": {
					"path": "param:fullpath",
					"url": "param:url",
					"title": "param:title"
				}
			}
		],
		"window_class": "Chrome_WidgetWin_1",
		"object_text": "param:object_name",
		"object_role": "ROLE_SYSTEM_WINDOW",
		"object_class": "Chrome_RenderWidgetHostHWND",
		"version": 0,
		"object_type": "HTMLObject",
		"object_flavor": "Link",
		"object_name": "First Blog Post",
		"object_library": "Chrome HTML",
		"window_name": "TTC Test Site – Just another WordPress site",
		"xpath": "//article[@id='post-15']/header/h2/a",
		"title": "TTC Test Site – Just another WordPress site",
		"url": "http://localhost:8080/",
		"fullpath": {
			"path": [
				{
					"tagName": "a",
					"attributes": [
						{
							"name": "href",
							"value": "http://localhost:8080/?p=15"
						},
						{
							"name": "rel",
							"value": "bookmark"
						}
					],
					"indexInParent": 0,
					"level": 0,
					"text": "First Blog Post"
				},
				{
					"tagName": "h2",
					"attributes": [
						{
							"name": "class",
							"value": "entry-title"
						}
					],
					"indexInParent": 0,
					"level": 1
				},
				{
					"tagName": "header",
					"attributes": [
						{
							"name": "class",
							"value": "entry-header"
						}
					],
					"indexInParent": 0,
					"level": 2
				},
				{
					"tagName": "article",
					"attributes": [
						{
							"name": "id",
							"value": "post-15"
						},
						{
							"name": "class",
							"value": "post-15 post type-post status-publish format-standard hentry category-uncategorized"
						}
					],
					"indexInParent": 2,
					"level": 3
				},
				{
					"tagName": "main",
					"attributes": [
						{
							"name": "id",
							"value": "main"
						},
						{
							"name": "class",
							"value": "site-main"
						},
						{
							"name": "role",
							"value": "main"
						}
					],
					"indexInParent": 0,
					"level": 4
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "primary"
						},
						{
							"name": "class",
							"value": "content-area"
						}
					],
					"indexInParent": 0,
					"level": 5
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "content"
						},
						{
							"name": "class",
							"value": "site-content"
						}
					],
					"indexInParent": 0,
					"level": 6
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "class",
							"value": "site-inner"
						}
					],
					"indexInParent": 0,
					"level": 7
				},
				{
					"tagName": "div",
					"attributes": [
						{
							"name": "id",
							"value": "page"
						},
						{
							"name": "class",
							"value": "site"
						}
					],
					"indexInParent": 0,
					"level": 8
				},
				{
					"tagName": "body",
					"attributes": [
						{
							"name": "class",
							"value": "home blog logged-in admin-bar hfeed customize-support"
						}
					],
					"indexInParent": 0,
					"level": 9
				},
				{
					"tagName": "html",
					"attributes": [
						{
							"name": "lang",
							"value": "en-US"
						},
						{
							"name": "class",
							"value": "js"
						},
						{
							"name": "toscacontainsshadowdom",
							"value": "false"
						},
						{
							"name": "openrequests",
							"value": "0"
						}
					],
					"indexInParent": 0,
					"level": 10
				}
			]
		},
		"screenshot": "Reports\\4023c592-bc8c-0045-16f0-a8278fee7480.png"
	}
}