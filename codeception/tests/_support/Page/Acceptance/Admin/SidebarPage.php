<?php
namespace Page\Acceptance\Admin;

class SidebarPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */
    public static $menuPosts = '//* [@id="menu-posts"]';
    public static $menuMedia = '//* [@id="menu-media"]';
    public static $menuAppearance = '//* [@id="menu-appearance"]';
    public static $menuPlugins = '//* [@id="menu-plugins"]';
    public static $menuSettings = '//* [@id="menu-settings"]';
    
    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static function getMenuSubItem($topLevel, $subMenuName)
    {
        return SELF::$$topLevel . '//a [text()[contains(.,"' . $subMenuName . '")]]';
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

    public function navigate($topLevel, $subMenuName)
    {
        $I = $this->acceptanceTester;
        $topLevel = 'menu' . $topLevel; 
        
        $I->moveMouseOver(SELF::$$topLevel);
        $subMenuXpath = SELF::getMenuSubItem($topLevel, $subMenuName);
        $I->waitForElementVisible($subMenuXpath);
        $I->click($subMenuXpath);
    }

}
