<?php
namespace Page\Acceptance\Admin;

class MediaLibraryPage
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */
    public static $imageThumbnails = '//* [@class="thumbnail"]';

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    public static function getSpecificImageThumbnailByName($name)
    {
        return SELF::$imageThumbnails . '//img [contains(@src, "' . $name . '")]';
    }

    public static function getThumbnailDivByName($name)
    {
        return SELF::getSpecificImageThumbnailByName($name) . '/ancestor::div [@class="thumbnail"]';
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

}
