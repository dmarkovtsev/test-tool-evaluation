<?php
namespace Page\Acceptance\Installer;

class Step2Page
{
    // include url of current page
    public static $URL = '';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    public static $title = '//*[@id="weblog_title"]';
    public static $user = '//*[@id="user_login"]';
    public static $password = '//*[@id="pass1-text"]';
    public static $confirmWeakPassword = '//*[@name="pw_weak"]';
    public static $email = '//*[@id="admin_email"]';
    public static $installWordpress = '//*[@id="submit"]';

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

}
