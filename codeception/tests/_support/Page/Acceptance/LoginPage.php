<?php
namespace Page\Acceptance;

class LoginPage
{
    // include url of current page
    public static $URL = 'wp-login.php';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */
    public static $username = '//*[@id="user_login"]';
    public static $password = '//*[@id="user_pass"]';
    public static $login = '//*[@id="wp-submit"]';

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }


    public function loginToWordpress($user = "wp_admin", $pass = "Testing123")
    {
        $I = $this->acceptanceTester;
        $I->fillField(SELF::$username, $user);
        $I->fillField(SELF::$password, $pass);
        sleep(1);
        $I->click(SELF::$login);
    }
}
