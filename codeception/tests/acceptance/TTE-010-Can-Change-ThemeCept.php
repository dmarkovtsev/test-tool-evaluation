<?php 
use \Page\Acceptance\AdminHeaderPage;
use \Page\Acceptance\Admin\SidebarPage;
use \Page\Acceptance\Admin\ThemesPage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Change the Theme and Confirm Different CSS Loaded');

$I->login();

// save current theme 
$I->click(AdminHeaderPage::$siteNameLink);
$previousCSS = $I->grabPageSource();

$I->amOnPage('/wp-admin/');
sleep(2);
$I->click(SidebarPage::$menuAppearance);
$lastThemeDivXPath = ThemesPage::getLastThemeDiv();
$I->moveMouseOver($lastThemeDivXPath);
$I->click(ThemesPage::$modalActivate);

$I->click(AdminHeaderPage::$siteNameLink);
$newCSS = $I->grabPageSource();

$I->assertNotEquals($previousCSS, $newCSS);

$I->logout();