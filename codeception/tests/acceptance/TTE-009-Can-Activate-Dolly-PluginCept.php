<?php 
use \Page\Acceptance\Admin\InstalledPluginsPage;
use \Page\Acceptance\Plugin\DollyPage;
use \Page\Acceptance\AdminHeaderPage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Activate Hello Dolly Plugin and Validate it Works');

$I->login();

$I->navigateAdminSidebar('Plugins', 'Installed Plugins');

$I->click(InstalledPluginsPage::getActivateLinkByPluginName('hello-dolly'));

$dolly = $I->grabTextFrom(DollyPage::$output);
$dolly = str_replace('’', "'", $dolly);
$I->assertContains($dolly, DollyPage::$lyrics);

$I->click(AdminHeaderPage::$new);

$dolly = $I->grabTextFrom(DollyPage::$output);
$dolly = str_replace('’', "'", $dolly);
$I->assertContains($dolly, DollyPage::$lyrics);

$I->logout();
