<?php 
use \Page\Acceptance\AdminHeaderPage;
use \Page\Acceptance\HomePage;
use \Page\Acceptance\SpecificPostPage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Comment on A Blog');

$I->login();

$I->click(AdminHeaderPage::$siteNameLink);

$I->click(HomePage::getBlogByTitle('Hello world!'));

$newComment = "foobar this is a comment " . time();

$I->fillField(SpecificPostPage::$newComment, $newComment);
$I->click(SpecificPostPage::$postComment);

$I->see($newComment, SpecificPostPage::$commentsDiv);
$I->see(date('F j, Y'), SpecificPostPage::$commentsDiv);

$I->logout();