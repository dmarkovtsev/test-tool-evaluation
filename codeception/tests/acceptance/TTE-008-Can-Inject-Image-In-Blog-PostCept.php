<?php 
use \Page\Acceptance\Admin\AddNewPostPage;
use \Page\Acceptance\Admin\MediaLibraryPage;
use \Page\Acceptance\AdminHeaderPage;
use \Page\Acceptance\SpecificPostPage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Inject Image from Media Library into Blog');

$I->login();

$I->navigateAdminSidebar('Posts', 'Add New');

$I->click(AddNewPostPage::$addMedia);

$I->click(AddNewPostPage::$fromMediaLibrary);
$I->click(MediaLibraryPage::getThumbnailDivByName('image'));
$I->click(AddNewPostPage::$insertIntoPost);

$I->click(AddNewPostPage::$publishButton);
$I->waitForText('Published');
$I->see('Published');

$I->click(AdminHeaderPage::$viewPage);
$I->seeElement(SpecificPostPage::getImageInContentByName('image'));

$I->logout();
