<?php 
use \Page\Acceptance\Admin\WelcomePanelPage;
use \Page\Acceptance\Admin\SettingsDiscussionPage;
use \Page\Acceptance\Admin\AddNewPostPage;
use \Page\Acceptance\AdminHeaderPage;
use \Page\Acceptance\SpecificPostPage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Disable Comments on New Posts and Confirm They Are Disabled');

$I->login();

// Disable comments for new posts
$I->click(WelcomePanelPage::$disableComments);

$I->uncheckOption(SettingsDiscussionPage::$allowCommentsOnNew);
$I->dontSeeCheckboxIsChecked(SettingsDiscussionPage::$registerToComment);
$I->uncheckOption(SettingsDiscussionPage::$previousCommentToAvoidModeration);

$I->click(SettingsDiscussionPage::$saveChanges);

// Make new post
$I->click(AdminHeaderPage::$new);

$I->fillField(AddNewPostPage::$title, 'Can\'t Comment Here');
$I->click(AddNewPostPage::$text);

$I->fillField(AddNewPostPage::$textContent, 'Don\'t comment on me.');

$I->click(AddNewPostPage::$publishButton);
$I->waitForText('Published');
$I->see('Published');

$I->click(AdminHeaderPage::$viewPage);

// Verify New Comment Div Not Present
$I->dontSeeElement(SpecificPostPage::$respondDiv);

$I->logout();