<?php 
use \Page\Acceptance\Admin\GeneralSettingsPage;
use \Page\Acceptance\AdminHeaderPage;
use \Page\Acceptance\HomePage;

$I = new \Step\Acceptance\WordpressStep($scenario);
$I->wantTo('Change Site Title in General Settings');

$I->login();

$I->navigateAdminSidebar('Settings', 'General');

$I->fillField(GeneralSettingsPage::$siteTitle, 'TTE Site Title');
$I->click(GeneralSettingsPage::$saveChanges);

$I->see('TTE Site Title', AdminHeaderPage::$headerDiv);
$I->click(AdminHeaderPage::$siteNameLink);

$I->see('TTE Site Title', HomePage::$siteTitle);

$I->logout();