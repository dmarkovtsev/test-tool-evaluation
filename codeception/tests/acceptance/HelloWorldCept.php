<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('Load Wordpress Install Page and Confirm Basic Operations are working.');
$I->amOnPage('/');
$I->seeInCurrentUrl('wp-admin/install.php');
$I->see('English');
